package pages;

import data.AdData;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
public class MemoPage extends BasePage {

    public MemoPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "tr[id^='tr_']")
    private List<WebElement> favoriteAds;

    public Optional<AdData> getAd(String id) {
        List<WebElement> favorites = driver.findElements(By.id(id));
        if (favorites.size() == 1) {
            String title = favorites.get(0).findElement(By.className("msg2")).getText().substring(0, 30);
            AdData ad = AdData.builder()
                    .id(id)
                    .title(title)
                    .build();
            return Optional.of(ad);
        } else if (favorites.size() > 1) {
            throw new RuntimeException("Found more than 1 Advertisement with id " + id);
        } else {
            return Optional.empty();
        }
    }

    public Set<AdData> getAllAds() {
        Set<AdData> ads = new HashSet<>();
        for (WebElement ad : favoriteAds) {
            String id = ad.getAttribute("id");
            String title = ad.findElement(By.className("msg2")).getText().substring(0, 30);
            ads.add(AdData.builder().id(id).title(title).build());
        }
        return ads;
    }

}
