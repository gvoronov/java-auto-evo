package pages;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.awaitility.Awaitility.await;

@Slf4j
public abstract class BasePage extends BaseHelper {

    public BasePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#main_table a > img.page_header_logo")
    private WebElement announcementsLink;

    @FindBy(css = "#main_table a[href$='/new/'].a_menu")
    private WebElement submitAnnouncementLink;

    @FindBy(css = "#main_table a[href$='/login/'].a_menu")
    private WebElement myMessagesLink;

    @FindBy(css = "#main_table a[href$='/search/'].a_menu")
    private WebElement searchLink;

    @FindBy(css = "#main_table a[href$='/favorites/'].a_menu")
    private WebElement memoLink;

    @FindBy(css = "#alert_dv #alert_ok")
    protected WebElement approveAlertBoxBtn;

    public AdsPage goToAnnouncementsPage() {
        log.info("Going to 'Announcements' page by Clicking on logo");
        announcementsLink.click();
        return new AdsPage(driver);
    }

    public SubmitAdPage goToSubmitAnnouncementPage() {
        log.info("Going to 'Submit Announcement' page");
        submitAnnouncementLink.click();
        return new SubmitAdPage(driver);
    }

    public MyMessagesPage goToMyMessagesPage() {
        log.info("Going to 'My Messages' page");
        myMessagesLink.click();
        return new MyMessagesPage(driver);
    }

    public SearchPage goToSearchPage() {
        log.info("Going to 'Search' page");
        searchLink.click();
        return new SearchPage(driver);
    }

    public MemoPage goToMemoPage() {
        log.info("Going to 'Memo' page");
        scrollUp();
        await().until(() -> isDisplayed(memoLink));
        memoLink.click();
        return new MemoPage(driver);
    }

}
