package pages;

import categories.SubCategory;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Slf4j
public class SubCategoryPage extends BasePage {

    public SubCategoryPage(WebDriver driver) {
        super(driver);
    }

    public SearchResultsPage clickSubCategory(SubCategory subCategory) {
        log.info("Clicking on '{}' subcategory link", subCategory);
        driver.findElement(By.id(subCategory.id())).click();
        return new SearchResultsPage(driver);
    }

}
