package pages;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.awaitility.Awaitility.await;

@Slf4j
public class AdPage extends BasePage {

    public AdPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "a_fav")
    private WebElement addToFavoritesLink;

    public AdPage clickAddToFavorites() {
        scrollDown();
        log.info("Clicking on 'Add to Favorites' link");
        await().until(() -> isDisplayed(addToFavoritesLink));
        addToFavoritesLink.click();
        log.info("Clicking on 'Ok' button of Alert box");
        await().until(() -> isDisplayed(approveAlertBoxBtn));
        approveAlertBoxBtn.click();
        return this;
    }

}
