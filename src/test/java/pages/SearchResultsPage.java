package pages;

import data.AdData;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Set;

import static org.awaitility.Awaitility.await;

@Slf4j
public class SearchResultsPage extends BasePage {

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//tr[starts-with(@id, 'tr_') and .//input[@name='mid[]']]")
    private List<WebElement> advertisements;

    @FindBy(id = "a_fav_sel")
    private WebElement addSelectedToFavoritesLink;

    public AdPage goToAd(String id) {
        log.info("Clicking on Announcement link");
        driver.findElement(By.id(id)).findElement(By.cssSelector(".msg2 .d1")).click();
        return new AdPage(driver);
    }

    public AdData getAdByIndex(int index) {
        WebElement announcement = advertisements.get(index);
        String id = announcement.getAttribute("id");
        String title = announcement.findElement(By.className("msg2")).getText().substring(0, 30);
        return AdData.builder()
                .id(id)
                .title(title)
                .build();
    }

    public SearchResultsPage selectAds(Set<AdData> ads) {
        log.info("Selecting announcements");
        for (AdData ad : ads) {
            driver.findElement(By.id(ad.getId())).findElement(By.cssSelector("input[type='checkbox']")).click();
        }
        return this;
    }

    public SearchResultsPage clickAddSelectedToFavorites() {
        scrollDown();
        log.info("Clicking on 'Add to Favorites' link");
        await().until(() -> isDisplayed(addSelectedToFavoritesLink));
        addSelectedToFavoritesLink.click();
        log.info("Clicking on 'Ok' button of Alert box");
        await().until(() -> isDisplayed(approveAlertBoxBtn));
        approveAlertBoxBtn.click();
        return this;
    }

}
