package pages;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Slf4j
public class AdsPage extends BasePage {

    public AdsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#dv_1 h2")
    private WebElement jobAndBusinessLink;

    @FindBy(css = "#dv_2 h2")
    private WebElement productionWorkLink;

    @FindBy(css = "#dv_3 h2")
    private WebElement realEstateLink;

    @FindBy(css = "#dv_4 h2")
    private WebElement animalsLink;

    @FindBy(css = "#dv_5 h2")
    private WebElement transportLink;

    @FindBy(css = "#dv_6 h2")
    private WebElement electronicsLink;

    @FindBy(css = "#dv_7 h2")
    private WebElement agricultureLink;

    @FindBy(css = "#dv_8 h2")
    private WebElement homeStuffLink;

    @FindBy(css = "#dv_9 h2")
    private WebElement constructionLink;

    @FindBy(css = "#dv_10 h2")
    private WebElement entertainmentLink;

    @FindBy(css = "#dv_11 h2")
    private WebElement forChildrenLink;

    @FindBy(css = "#dv_12 h2")
    private WebElement clothesFootwearLink;

    public CategoryPage goToTransport() {
        log.info("Clicking on 'Transport' category link");
        transportLink.click();
        return new CategoryPage(driver);
    }

    public CategoryPage goToElectronics() {
        log.info("Clicking on 'Electronics' category link");
        electronicsLink.click();
        return new CategoryPage(driver);
    }

}
