package pages;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

@Slf4j
public abstract class BaseHelper {

    protected WebDriver driver;
    protected JavascriptExecutor js;

    public BaseHelper(WebDriver driver) {
        this.driver = driver;
        js = (JavascriptExecutor) driver;
        PageFactory.initElements(driver, this);
    }

    public void scrollUp() {
        log.info("Scrolling Up");
        js.executeScript("window.scrollBy(0, -document.body.scrollHeight);");
    }

    public void scrollDown() {
        log.info("Scrolling Down");
        js.executeScript("window.scrollBy(0, document.body.scrollHeight);");
    }

    public void scrollIntoView(WebElement element) {
        js.executeScript("arguments[0].scrollIntoView();", element);
    }

    protected boolean isDisplayed(By locator) {
        try {
            return driver.findElement(locator).isDisplayed() && driver.findElement(locator).isEnabled();
        } catch (NoSuchElementException ex) {
            return false;
        }
    }

    protected boolean isDisplayed(WebElement element) {
        try {
            return element.isDisplayed() && element.isEnabled();
        } catch (NoSuchElementException ex) {
            return false;
        }
    }

}
