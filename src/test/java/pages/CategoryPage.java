package pages;

import categories.Category;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Slf4j
public class CategoryPage extends BasePage {

    public CategoryPage(WebDriver driver) {
        super(driver);
    }

    public SubCategoryPage clickCategory(Category category) {
        log.info("Clicking on '{}' category link", category);
        driver.findElement(By.id(category.id())).click();
        return new SubCategoryPage(driver);
    }
}
