package data;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AdData {

    private String id;
    private String title;

}
