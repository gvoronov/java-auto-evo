package categories;

public enum Cars implements SubCategory {
    AlfaRomeo {
        public String id() {
            return "ahc_99";
        }
    },
    Audi {
        public String id() {
            return "ahc_103";
        }
    },
    Bmw {
        public String id() {
            return "ahc_106";
        }
    }
}
