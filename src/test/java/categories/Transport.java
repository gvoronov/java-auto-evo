package categories;

public enum Transport implements Category {
    Cars {
        public String id() {
            return "ahc_97";
        }
    },
    CargoCars {
        public String id() {
            return "ahc_180";
        }
    }
}
