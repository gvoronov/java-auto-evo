package utils;

import static org.openqa.selenium.remote.BrowserType.FIREFOX;

public class ConfigurationUtil {

    public static String getBrowser() {
        return System.getProperty("browser", FIREFOX);
    }

}
