package tests;

import categories.Cars;
import categories.Transport;
import data.AdData;
import org.testng.annotations.Test;
import pages.MemoPage;
import pages.SearchResultsPage;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class MemoPageTest extends BaseTest {

    @Test(description = "Open advertisement, add it to Favorites, Verify the ad is present on Memo page")
    public void addOneAdToFavorites() {

        SearchResultsPage searchResultsPage = openAdsPage()
                .goToTransport()
                .clickCategory(Transport.Cars)
                .clickSubCategory(Cars.Bmw);

        AdData ad = searchResultsPage.getAdByIndex(0);

        MemoPage memoPage = searchResultsPage
                .goToAd(ad.getId())
                .clickAddToFavorites()
                .goToMemoPage();

        assertThat(memoPage.getAd(ad.getId()))
                .isEqualTo(Optional.of(ad));
    }

    @Test(description = "Search for Ads, add two Ads to Favorites, Verify the ads are present on Memo page")
    public void addTwoAdsToFavorites() {
        SearchResultsPage searchResultsPage = openAdsPage()
                .goToTransport()
                .clickCategory(Transport.Cars)
                .clickSubCategory(Cars.Bmw);

        Set<AdData> ads = new HashSet<>();
        ads.add(searchResultsPage.getAdByIndex(0));
        ads.add(searchResultsPage.getAdByIndex(1));

        MemoPage memoPage = searchResultsPage
                .selectAds(ads)
                .clickAddSelectedToFavorites()
                .goToMemoPage();

        assertThat(memoPage.getAllAds())
                .isEqualTo(ads);
    }

}
