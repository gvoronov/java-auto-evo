package tests;

import domain.Constant;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import pages.AdsPage;
import utils.ConfigurationUtil;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.remote.BrowserType.CHROME;
import static org.openqa.selenium.remote.BrowserType.FIREFOX;

@Slf4j
public class BaseTest {

    private WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        switch (ConfigurationUtil.getBrowser()) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
        }
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Constant.BASE_URL);
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        driver.quit();
    }

    @BeforeMethod
    public void beforeMethod(Method method) {
        log.info("Start test: {}", method.getName());
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(Method method) {
        log.info("End test: {}", method.getName());
    }

    protected AdsPage openAdsPage() {
        return new AdsPage(driver).goToAnnouncementsPage();
    }

}
