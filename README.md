# Automation Project

Automation project for testing www.ss.com website.

## Usage

##### Possible browser options:
- Firefox
- Chrome

##### Tests can be run using:
- TestNG (running from IDE)
    - Setting:```-Dbrowser=chrome``` 
- Gradle Wrapper by typing command in Terminal/CMD:
    - All tests ```./gradlew test```(Mac) and ```gradlew test```(Win)
    - Specific *.xml config ```./gradlew testMemo```
        - With browser setting: ```./gradlew -Pbrowser=chrome testMemo```

## Main Tech Stack

- Java 8
- Gradle
- Selenium
- TestNG
- AssertJ
- Logback